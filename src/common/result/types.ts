/**
 * Response result
 */
export type Result<T> = {
	status: Status;
	result: T;
	error?: string;
}

/**
 * Response status
 */
export enum Status {
	Success = 'success',
	Error = 'error'
}
