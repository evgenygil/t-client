import React from 'react';

import {avatarStyle, dateStyle, messageWrapperStyle, userStyle} from './styles';

import {MessageProps} from './types';

/**
 * Message component
 */
export const Message = React.memo(({content, user, datetime}: MessageProps) => (
	<div style={{paddingBottom: 12}}>
		<div style={messageWrapperStyle}>
			<div style={avatarStyle}>
				<img
					src={user.avatarUrl}
					style={{height: 30}}
					alt={user.name}
				/>
			</div>
			<div>
				<span style={userStyle}>{user.name}</span>
				<div style={dateStyle}>{new Date(datetime).toLocaleString()}</div>
				<div>{content}</div>
			</div>
		</div>
	</div>
))
