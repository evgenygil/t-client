export const userStyle = {
	fontSize: 14,
	color: '#333333'
}

export const dateStyle = {
	fontSize: 12,
	color: '#888888'
}

export const messageWrapperStyle = {
	display: 'flex',
	backgroundColor: '#f7f7f9',
	padding: '8px 12px',
	border: '1px solid #f1f1f1',
	borderRadius: '5px'
}

export const avatarStyle = {
	marginRight: 12
}
