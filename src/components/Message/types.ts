import {User} from '../../api/user/types';

export type MessageProps = {
	content: string;
	user: User;
	datetime: Date;
}
