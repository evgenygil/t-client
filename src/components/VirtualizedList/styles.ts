import {CSSProperties} from 'react';

export const getWrapperStyles = (windowHeight: number | string): CSSProperties => ({
	overflowY: 'scroll',
	height: windowHeight,
	padding: 5,
	willChange: 'scroll-position'
})

export const getElementsBlockStyle = (innerHeight: number): CSSProperties => ({
	position: 'relative',
	height: `${innerHeight}px`
})
