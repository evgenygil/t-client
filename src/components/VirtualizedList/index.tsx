import React, {
	ComponentProps,
	SyntheticEvent,
	createRef,
	useEffect,
	useMemo,
	useState, MutableRefObject
} from 'react';

import {useResizeObserver} from '../../common/hooks/use-resize-observer';

import {
	DEFAULT_OVERSCAN,
	ITEM_DEFAULT_HEIGHT,
	WINDOW_DEFAULT_HEIGHT
} from './constants';

import {VirtualizedListProps} from './types';

import {getElementsBlockStyle, getWrapperStyles} from './styles';

/**
 * Virtualized list
 */
export const VirtualizedList = React.memo(({
	renderItem,
	overscan = DEFAULT_OVERSCAN,
	windowHeight,
	items,
	detectResize = false
}: VirtualizedListProps) => {
	const renderedItems: ComponentProps<any>[] = [];

	const [scrollTop, setScrollTop] = useState(0);
	const [offsetArray, setOffsetArray] = useState<number[]>([0]);
	const [itemAvgHeight, setItemAvgHeight] = useState<number>(ITEM_DEFAULT_HEIGHT);
	const [windowOffsetHeight, setWindowOffsetHeight] = useState<number>(WINDOW_DEFAULT_HEIGHT);

	const [wrapperRef, wrapperWidth, wrapperHeight] = useResizeObserver();

	const listLength = useMemo(() => items.length, [items]);

	const innerHeight = listLength * itemAvgHeight;
	const startIndex = Math.floor(scrollTop / itemAvgHeight);
	const endIndex = Math.min(listLength - 1, Math.floor((scrollTop + windowOffsetHeight) / itemAvgHeight));

	const startIndexOverScanned = useMemo(() => startIndex > overscan
		? startIndex - overscan
		: startIndex, [startIndex]);

	const endIndexOverScanned = useMemo(() => endIndex > listLength - 1 - overscan
		? endIndex
		: endIndex + overscan, [endIndex]);

	for (let i = startIndexOverScanned; i <= endIndexOverScanned; i++) {
		const ref = createRef();

		renderedItems.push(renderItem({
			index: i,
			style: {
				position: 'absolute',
				transform: `translate(0px, ${offsetArray[i]}px)`
			},
			ref
		}));
	}

	useEffect(() => {
		setWindowOffsetHeight((wrapperRef as MutableRefObject<HTMLDivElement>).current.offsetHeight)
	}, [])

	useEffect(() => {
		const itemsOffsets = renderedItems.reduce((acc, item) => {
			const n = 1 * item.key + 1

			if (!detectResize && offsetArray[n]) {
				return acc;
			}

			acc[n] = acc[n - 1] + item.ref.current.offsetHeight

			return acc
		}, offsetArray)

		itemsOffsets.pop()

		setItemAvgHeight(Math.ceil(itemsOffsets[itemsOffsets.length - 1] / itemsOffsets.length))

		setOffsetArray([...itemsOffsets]);
	}, [scrollTop, wrapperWidth, wrapperHeight])

	const onScroll = (e: SyntheticEvent) => setScrollTop(e.currentTarget.scrollTop);

	return (
		<>
			<div
				style={getWrapperStyles(windowHeight)}
				onScroll={onScroll}
				ref={wrapperRef as MutableRefObject<HTMLDivElement>}
			>
				<div style={getElementsBlockStyle(innerHeight)}>
					{renderedItems}
				</div>
			</div>
		</>
	);
});
