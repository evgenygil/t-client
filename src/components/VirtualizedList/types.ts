import React, {ComponentProps, ReactElement} from 'react';

export type VirtualizedListProps = {
	/** Function to render element */
	renderItem: (itemProps: RenderItemProps) => ReactElement<HTMLElement>;
	/** Outer window height */
	windowHeight: number | string;
	/** Array of rendering elements */
	items: any[];
	/** How many items before last, starting to render next item's chunk */
	overscan?: number;
	/** Detect width/height changing and recalculate items offsets */
	detectResize?: boolean;
}

export type RenderedItem = ComponentProps<any>;

export type RenderItemProps = {
	/** Element position */
	index: number;
	/** Element style */
	style: {[key: string]: string};
	/** Reference to calculate item properties */
	ref: React.RefObject<any>;
}
