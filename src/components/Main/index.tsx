import * as React from 'react';

import {MessageList} from '../MessageList';

export const Main = React.memo(() => (
	<MessageList />
));
