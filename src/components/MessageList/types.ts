import {Message} from '../../api/message/types';

export type MessageListProps = {
	messages: Message[]
}
