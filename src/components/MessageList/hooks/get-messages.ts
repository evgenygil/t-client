import {useEffect, useState} from 'react'

import {messageService} from '../../../api';

import {Message} from '../../../api/message/types';
import {Status} from '../../../common/result/types';

const MESSAGES_PER_PAGE = 100;

/**
 * Fetch messages hook
 */
export const useGetMessages = (page: number) => {
	const [messages, setMessages] = useState<Message[]>([])

	useEffect(() => {
		messageService
			.getMessages({
				offset: MESSAGES_PER_PAGE * (page - 1),
				limit: MESSAGES_PER_PAGE
			})
			.then((res) => {
				const {data} = res;

				if (data.status === Status.Success) {
					setMessages((prevMessages) => {
						return [...prevMessages, ...data.result]
					});
				} else {
					console.log('Error in result data.');
				}
			})
			.catch((e) => {
				console.log(e);
			})
	}, [page])

	return messages
}
