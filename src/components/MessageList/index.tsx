import React, {
	useCallback,
	useRef,
	useState
} from 'react';

import {VirtualizedList} from '../VirtualizedList';
import {Message} from '../Message';

import {PRELOAD_OVERSCAN_VALUE} from './constants';

import {useGetMessages} from './hooks/get-messages';

/**
 * List of chat messages
 */
export const MessageList = React.memo(() => {
	const [pageNumber, setPageNumber] = useState<number>(1)
	const observer = useRef<IntersectionObserver>()

	const messages = useGetMessages(pageNumber)

	const lastMessageElementRef = useCallback((node) => {
		if (observer.current) {
			observer.current.disconnect()
		}

		observer.current = new IntersectionObserver(([entry]) => {
			if (entry.isIntersecting) {
				setPageNumber(prevPageNumber => prevPageNumber + 1)
			}
		})

		if (node) {
			observer.current.observe(node)
		}
	}, [])

	return (
		<div>
			<h2>jb-chat-client</h2>
			{
				messages.length > 0
					? <VirtualizedList
						items={messages}
						windowHeight='80vh'
						detectResize
						renderItem={({index, style, ref}) => {
							const message = messages[index];

							return (
								<div
									key={index}
									style={style}
									ref={ref}
								>
									<Message {...message} />
									{
										(messages.length === index + 1 + PRELOAD_OVERSCAN_VALUE) &&
										<span ref={lastMessageElementRef}/>
									}
								</div>
							)
						}}
					/>
					: <div>Empty list</div>
			}
		</div>
	)
})
