import React from 'react'

import {Main} from '../../components/Main';

const mainStyle = {
	padding: 18
}

export const App = () => (
	<div style={mainStyle}>
		<Main/>
	</div>
)
