import {User} from '../user/types';

export type Message = {
	id: string;
	content: string;
	user: User;
	datetime: Date;
}

export type GetMessagesRequest = {
	limit?: number;
	offset?: number;
}
