import {ApiResponse} from '../types';
import {getRequest} from '../index';
import {GetMessagesRequest, Message} from './types';
import {Result} from '../../common/result/types';

const getMessages = (params?: GetMessagesRequest): ApiResponse<Result<Message[]>> => (
	getRequest('/message/list', {params})
);

export const messageService = {
	getMessages
};
