import axios, {AxiosRequestConfig} from 'axios';
import {config} from '../config/api';

const api = axios.create({
	baseURL: config.apiEndpoint
});

export const getRequest = (url: string, config?: AxiosRequestConfig) => api.get(url, config);

export * from './message';
